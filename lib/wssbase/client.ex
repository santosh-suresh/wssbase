defmodule Wssbase.Client do
  use WebSockex

  @url "wss://ws-feed.pro.coinbase.com"
  def start_link(products \\ []) do
    {:ok, pid} = WebSockex.start_link(@url, __MODULE__, :no_state)
    subscribe(pid, products)
    {:ok, pid}
  end

  def handle_connect(_conn, state) do
    IO.puts("connected!")
    {:ok, state}
  end

  def handle_frame({:text, msg}, state) do
    msg
    |> Jason.decode!()
    |> handle_message(state)
  end

  defp handle_message(%{"type" => "match"} = trade, state) do
    IO.inspect(trade)
    {:ok, state}
  end

  defp handle_message(_, state) do
    {:ok, state}
  end

  defp subscribe(pid, products) do
    WebSockex.send_frame(pid, create_subscription_frame(products))
  end

  defp create_subscription_frame(products) do
    subscription_msg =
      %{
        type: "subscribe",
        product_ids: products,
        channels: ["matches"]
      }
      |> Jason.encode!()

    {:text, subscription_msg}
  end
end
