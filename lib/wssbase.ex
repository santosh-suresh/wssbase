defmodule Wssbase do
  @moduledoc """
  Documentation for Wssbase.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Wssbase.hello()
      :world

  """
  def hello do
    :world
  end
end
